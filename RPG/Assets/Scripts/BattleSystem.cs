﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BattleSystem : MonoBehaviour
{
    public List<Unit> playerUnits;
    public List<Unit> enemyUnits;

    public List<Unit> actionQueue;
    public int queueIndex = 0;
    public Unit currentTurn;

    [Header("Timers")]
    [SerializeField] private float firstWait;
    [SerializeField] private float nextTurnWait;

    IEnumerator Start()
    {
        //Ordering action queue
        #region        
        List<Unit> allUnits = FindObjectsOfType<Unit>().ToList();

        foreach (Unit unit in allUnits)
        {
            if (unit.gameObject.tag == "Enemy") enemyUnits.Add(unit);
            else playerUnits.Add(unit);
        }

        for (int i = 0; i < playerUnits.Count; i++)
        {
            actionQueue.Add(playerUnits[i]);
            Debug.Log("Adding " + playerUnits[i].name);
        }

        for (int i = 0; i < enemyUnits.Count; i++)
        {
            actionQueue.Add(enemyUnits[i]);
            Debug.Log("Adding " + enemyUnits[i].name);
        }

        actionQueue = actionQueue.OrderByDescending(x => x.GetComponent<Unit>().speed).ToList();
        #endregion 
        yield return new WaitForSeconds(firstWait); // First wait time before first action of the battle
        actionQueue[queueIndex].ManageCharacterButtons(true, 0);
    }

    public IEnumerator EndCurrentUnitTurn()
    {
        yield return new WaitForSeconds(nextTurnWait);
        queueIndex++;
        if (queueIndex >= actionQueue.Count) queueIndex = 0;
        actionQueue[queueIndex].ManageCharacterButtons(true, 2);
    }
}
