﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Unit : MonoBehaviour
{
    public int level;
    public int healthPoints;
    public int magicPoints;
    public int attack;
    public int defense;
    public int magicPower;
    public int magicDefense;
    public int speed;
    [Header("PopUp")]
    public GameObject popUpPrefab;
    public float hOffset;
    public float vOffset;

    public List<Skill> skills;

    public List<Vector2> buttonsPosition;
    public List<Transform> positions;
    private Vector2 originalPosition;
    private Vector3 targetPosition;

    

    [Header("Animation")]
    private Animator animator;
    private SpriteRenderer sprite;
    public float moveSpeed;
    public float waitTimeBeforeAttack;
    public float waitTimeAfterAttack;

    private void Start()
    {
        originalPosition = transform.position;
        targetPosition = transform.position;
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);
    }

    public IEnumerator Attack(Vector2 moveTo, Unit attacked)
    {
        yield return new WaitForSeconds(waitTimeBeforeAttack);
        animator.Play("BattleAttackMove");
        targetPosition = attacked.positions[0].position;
        yield return new WaitUntil(() => transform.position == targetPosition);
        animator.Play("BattleAttack");
        DealDamage(attacked);
        yield return new WaitForSeconds(waitTimeAfterAttack);
        sprite.flipX = true;
        animator.Play("BattleAttackMove");
        targetPosition = originalPosition;
        yield return new WaitUntil(() => transform.position == targetPosition);
        sprite.flipX = false;
        animator.Play("BattleIdle");
        StartCoroutine(FindObjectOfType<BattleSystem>().EndCurrentUnitTurn());
    }

    public void ManageCharacterButtons(bool value, int mode)
    {
        foreach (ActionButton button in GetComponentsInChildren<ActionButton>(true))
        {
            if (mode == 0) button.gameObject.SetActive(value);

            if (mode == 1)
            {
                button.GetComponent<SpriteRenderer>().enabled = value;
                button.GetComponent<CircleCollider2D>().enabled = value;
            }

            if (mode == 2)
            {
                button.gameObject.SetActive(!value);
                button.GetComponent<SpriteRenderer>().enabled = value;
                button.GetComponent<CircleCollider2D>().enabled = value;
                button.gameObject.SetActive(value);
            }
        }
    }

    public void PopUpText(int text, Vector3 offset)
    {
        var textGameObject = Instantiate(popUpPrefab, transform.position + offset, transform.rotation);
        textGameObject.GetComponentInChildren<TMP_Text>().text = text.ToString();
    }

    public void DealDamage(Unit attacked)
    {
        int damage;
        damage = (attack - attacked.defense);
        if (damage <= 0)
        {
            damage = 1;
        }
        attacked.healthPoints -= damage;
        PopUpText(damage, new Vector3(hOffset, vOffset, 0));
        Debug.Log(gameObject.name + " attacked " + attacked.name + " dealing " + damage + " of damage!");
    }
}
