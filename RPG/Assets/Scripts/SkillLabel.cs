﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillLabel : MonoBehaviour
{
    [SerializeField] public Skill skill;
    [SerializeField] public TextMesh text;
    [SerializeField] public BoxCollider2D box;

    void Awake()
    {
        Debug.Log("Hi!");
        text = GetComponent<TextMesh>();
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(.25f);
        gameObject.AddComponent(typeof(BoxCollider2D));
    }

    void Update()
    {
        //set the text with the name of the skill
        text.text = skill.name; 
    }

    private void OnMouseDown()
    {
        Debug.Log("Using " + skill.name);
    }
}
