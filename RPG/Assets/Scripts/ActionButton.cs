﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionButton : MonoBehaviour
{
    public enum Action { Attack, Skill }
    public Action action;
    public bool canClick = true;
    public Unit unit;

    public bool canSelectTarget = false;
    public Unit unitTarget;
    public GameObject skillLabel;

    private void Start()
    {
        unit = GetComponentInParent<Unit>();     
    }

    private void Update()
    {
        SelectAttackTarget();
    }

    public IEnumerator AttackRoutine()
    {
        unit.ManageCharacterButtons(false, 1);
        canSelectTarget = true;
        yield return new WaitUntil(() => unitTarget);
        StartCoroutine(unit.Attack(unitTarget.transform.position /*+offset*/, unitTarget));
        canSelectTarget = false;
    }

    public IEnumerator SkillRoutine()
    {
        unit.ManageCharacterButtons(false, 1);
        Vector2 offset = new Vector2(0, 0);
        foreach (Skill skill in unit.skills)
        {
            Debug.Log("accessing list of skills of " + unit.name);
            var newSkillLabel = Instantiate(skillLabel, new Vector2(0, 0), transform.rotation);
            newSkillLabel.GetComponent<SkillLabel>().skill = unit.skills[0];
        }
        yield return new WaitUntil(() => unitTarget);
    }

    public void SelectAttackTarget()
    {
        unitTarget = ReturnUnitByRay();
    }

    public Unit ReturnUnitByRay()
    {
        RaycastHit2D raycast = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (canSelectTarget && Input.GetMouseButtonDown(0) && raycast.collider != null && raycast.collider.tag == "Enemy")
        {
            Debug.Log(raycast.collider.GetComponent<Unit>().name);
            return raycast.collider.GetComponent<Unit>();
        } 
        else return null;
    }

    private void OnMouseDown()
    {
        if (canClick)
        {
            if (action == Action.Attack)
            {
                StartCoroutine(AttackRoutine());
                Debug.Log("Clicked on " + action.ToString() + " button");
            }

            else if (action == Action.Skill)
            {
                StartCoroutine(SkillRoutine());
                Debug.Log("Clicked on " + action.ToString() + " button");
            }
        }
    }
}
